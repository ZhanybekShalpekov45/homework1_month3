import random
import os

from aiogram import Bot, Dispatcher, executor, types
from dotenv import load_dotenv
from os import getenv

load_dotenv()
bot = Bot(token=getenv('BOT_TOKEN'))
dp = Dispatcher(bot)

@dp.message_handler(commands=['start'])
async def start(message: types.Message):
    print(f"{message.from_user=}")
    await message.answer(f"Привет, {message.from_user.first_name}!")


@dp.message_handler(commands=['myinfo'])
async def myinfo(message: types.Message):
    user_id = message.from_user.id
    username = message.from_user.username
    name = message.from_user.first_name
    info = f'Ваш id = {user_id}\n' \
           f'Ваш никнейм : {username}\n' \
           f'Ваше имя : {name}'
    await message.answer(info)


@dp.message_handler(commands=['picture'])
async def send_picture(message: types.Message):
    photos_path = "images"
    photo_files = os.listdir(photos_path)
    random_photo = random.choice(photo_files)
    photos_path = os.path.join(photos_path, random_photo)
    with open(photos_path, 'rb') as photo_file:
        await bot.send_photo(chat_id=message.chat.id, photo=photo_file.read())

if __name__ == '__main__':
    executor.start_polling(dp)